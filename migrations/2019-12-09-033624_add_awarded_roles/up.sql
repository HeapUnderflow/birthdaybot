-- Your SQL goes here

CREATE TABLE IF NOT EXISTS "awarded_roles" (
	userid BIGINT NOT NULL,
	guildid BIGINT NOT NULL REFERENCES guilds(id),
	roleid BIGINT NOT NULL,
	remove_at TIMESTAMPTZ NOT NULL,
	PRIMARY KEY (userid, guildid)
);