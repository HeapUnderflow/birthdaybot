-- Your SQL goes here

CREATE TABLE IF NOT EXISTS "guilds" (
	id BIGINT NOT NULL PRIMARY KEY,
	announce_in BIGINT,
	announce_format TEXT NOT NULL,
	set_role BIGINT,
	remove_role BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS "birthdays" (
	userid BIGINT NOT NULL,
	guildid BIGINT NOT NULL REFERENCES guilds(id),
	birthday TIMESTAMPTZ NOT NULL,
	PRIMARY KEY(userid, guildid) -- have a composite pk here
);