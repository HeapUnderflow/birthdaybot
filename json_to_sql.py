#!/usr/bin/env python3

import json

MIGRATIONS = "./generated_migrations"
MIGRATIONS_IN = MIGRATIONS + "/birthdays.json"
MIGRATIONS_OUT = MIGRATIONS + "/birthdays.sql"

def convert_guild(f, id, guild_obj):
	print("converting guild {}".format(id))
	f.write("-- guild {}\n".format(id))
	f.write("INSERT INTO guilds (id, announce_in, announce_format, set_role, remove_role) VALUES ({}, {}, '{}', {}, '{}') ON CONFLICT DO NOTHING;\n".format(
		id,
		guild_obj["announce_in"] if guild_obj["announce_in"] is not None else "null",
		guild_obj["announce_format"].replace("\\", "\\\\").replace("'", "''"),
		guild_obj["grant_role"] if guild_obj["grant_role"] is not None else "null",
		guild_obj["remove_role"]
	))

	for bd in guild_obj["birthdays"]:
		convert_bday(f, id, bd)

def convert_bday(f, gid, bday_obj):
	print("converting bday {}".format(bday_obj["user"]))
	f.write("INSERT INTO birthdays (userid, guildid, birthday, tz) VALUES ({}, {}, '{}', {}) ON CONFLICT DO NOTHING;\n".format(
		bday_obj["user"],
		gid,
		bday_obj["date"] + " 00:01:00+00",
		0
	))
	pass

def convert_file(f, to):
	print("converting file {} to {}".format(f, to))
	with open(f, "r", encoding="utf-8") as r:
		data = json.load(r)
	with open(to, "w", encoding="utf-8") as w:
		w.write("BEGIN;\n")
		for i, g in data["guilds"].items():
			convert_guild(w, i, g)
		w.write("COMMIT;\n")

convert_file(MIGRATIONS_IN, MIGRATIONS_OUT)
