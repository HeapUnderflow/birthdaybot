<a name=""></a>
##  (unofficial) Keysight Seed DB (2021-01-09)


#### Features

* **intents:**  use gateway intents to reduce the bot load ([9419e313](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/9419e313bbd378987eaf40a9b45eebd475c268c0))

#### Bug Fixes

*   fix wrong cache arc passed to functions ([3585ce65](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/3585ce6539b05aebd7bbb5c7d15882748866d74c))
* **command checks:**  use the new check result type from serenity ([8e1b1cfa](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/8e1b1cfaad5a5455d0d30180f77bd1a1855d9821))



