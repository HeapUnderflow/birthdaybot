{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cacert binutils gcc gnumake openssl pkgconfig postgresql python37 python37Packages.pylint
  ];
}
