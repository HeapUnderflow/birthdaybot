# Birthday Bot

Discord bot that can announce birthdays and/or give roles for 24hrs on the birthday.

### Notes:

- The `BirthdayBot` role has the be higher then the role that the bot should give for a birthday
- Configuration has to happen in a channel BirthdayBot can see
- By default BirthdayBot does absolutely nothing, look at `bd! help` for instructions on how to configure the bot
- Permission Limitations
  - The Adding / Removing of birthdays is bound to the "Manage Roles" permission (everyone who can manage roles can add/remove bdays)
  - The configuring commands (`set` group) require that you have the Administrator permission
  - All other commands can be used by all

## Getting it

You can get it [here](https://discordapp.com/oauth2/authorize?client_id=625588618525802507&scope=bot&permissions=268569600)

## Running it yourself

It is confirmed to be working with rust 1.37, other versions might work too.
Just clone the repository and run `cargo build --release`.
