pub mod models;
pub mod schema;

use crate::START_OF_DAY;
use chrono::{FixedOffset, NaiveDate, TimeZone, Utc};
use diesel::{
	prelude::*,
	r2d2::ConnectionManager,
	result::{DatabaseErrorKind, Error as DieselError},
	PgConnection,
};
use models::{Birthday, U64};
use r2d2::Pool;
use std::{fmt::Debug, sync::Arc};
use tokio_diesel::{AsyncError, AsyncResult, AsyncRunQueryDsl};

#[derive(Debug, thiserror::Error)]
pub enum DbError {
	#[error("a connection error occured")]
	ConnectionError(#[from] diesel::ConnectionError),
	#[error("failed to retrieve a connection from the connection pool")]
	PoolError(#[from] r2d2::Error),
	#[error("the database url was not found in the environment")]
	MissingDbUrl,
}

pub type ConnectionPool = Arc<Pool<ConnectionManager<PgConnection>>>;

pub type DbResult<T> = Result<T, DbError>;

pub fn create_connection() -> DbResult<ConnectionPool> {
	let dburl = kankyo::key("DATABASE_URL").ok_or(DbError::MissingDbUrl)?;
	let conn = Pool::new(ConnectionManager::new(dburl)).map_err(|err| DbError::PoolError(err))?; // .context(ConnectionError)?
	Ok(Arc::new(conn))
}

#[tracing::instrument(skip(conn))]
#[inline]
pub async fn get_upcoming_birthdays<T>(
	conn: ConnectionPool,
	guild: T,
	count: i64,
) -> AsyncResult<Vec<Birthday>>
where
	T: Into<U64> + std::fmt::Debug,
{
	use schema::birthdays::dsl::*;

	tracing::debug!("retrieving upcoming birthdays");
	let gid = guild.into();
	birthdays
		.filter(guildid.eq(gid))
		.order_by(birthday)
		.limit(count)
		.load_async::<Birthday>(&*conn)
		.await
}

#[tracing::instrument(skip(conn))]
pub async fn add_birthday(
	conn: ConnectionPool,
	guild: impl Into<U64> + Debug,
	user: impl Into<U64> + Debug,
	date: NaiveDate,
	timezone: FixedOffset,
) -> Result<(), BirthdayError> {
	use schema::birthdays::dsl::*;

	tracing::debug!("adding birthday");
	// "shift" the datetime so that its in utc but still the same tod in the target
	// timezone
	let datetime = timezone
		.from_local_datetime(&date.and_hms(START_OF_DAY.0, START_OF_DAY.1, START_OF_DAY.2))
		.latest()
		.ok_or(BirthdayError::WhatTheFuckIsThisDate)?
		.with_timezone(&Utc);
	let gid = guild.into();
	let uid = user.into();

	let new_bday = Birthday {
		guildid:  gid,
		userid:   uid,
		birthday: datetime,
		// we seperatly store the timezone here, to later be able to construct
		// a "local" representation in output commands
		// as to avoid having 23:01:00+0000 and instead having 00:01:00-0001
		// and the likes
		tz:       timezone.local_minus_utc(),
	};

	let nbd = new_bday.clone();
	match tokio::task::spawn_blocking(move || {
		let con = conn.get().map_err(AsyncError::Checkout)?;
		diesel::insert_into(birthdays).values(&new_bday).execute(&*con).map_err(AsyncError::Error)
	})
	.await
	.expect("task panicked")
	{
		Ok(v) if v >= 1 => Ok(()),
		Ok(_) => Err(BirthdayError::InconsistentInsert),
		Err(e) => {
			tracing::trace!(val=?nbd, e=?e, "insert errored");
			match &e {
				AsyncError::Checkout(_) => Err(BirthdayError::Other { source: e }),
				AsyncError::Error(diesel_err) => match diesel_err {
					DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _) => {
						Err(BirthdayError::AlreadyExists)
					},
					DieselError::DatabaseError(DatabaseErrorKind::ForeignKeyViolation, _) => {
						Err(BirthdayError::NoGuildFound { gid: *gid })
					},
					_ => Err(BirthdayError::Other { source: e }),
				},
			}
		},
	}
}

#[tracing::instrument(skip(conn))]
#[inline]
pub async fn remove_birthday(conn: ConnectionPool, bd: Birthday) -> Result<(), BirthdayError> {
	tracing::debug!("removing birthday");
	match tokio::task::spawn_blocking(move || {
		let con = conn.get().map_err(AsyncError::Checkout)?;
		diesel::delete(&bd).execute(&*con).map_err(AsyncError::Error)
	})
	.await
	.expect("task panicked")
	{
		Ok(v) if v >= 1 => Ok(()),
		Ok(_) => Err(BirthdayError::BirthdayNotFound),
		Err(e) => Err(BirthdayError::Other { source: e }),
	}
}

#[derive(Debug, thiserror::Error)]
pub enum BirthdayError {
	#[error("the given birthday already exists")]
	AlreadyExists,
	#[error("the given birthday was not found")]
	BirthdayNotFound,
	#[error("got a absolutely crazy and invalid date")]
	WhatTheFuckIsThisDate,
	#[error("no guild with the id {gid} was found")]
	NoGuildFound { gid: u64 },
	#[error("the insert operation was inconsistent")]
	InconsistentInsert,
	#[error("a not specifically handled error occured: {source:?}")]
	Other {
		#[from]
		source: AsyncError,
	},
}
