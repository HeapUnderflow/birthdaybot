use super::schema::{awarded_roles, birthdays, guilds};
use chrono::{DateTime, Utc};
use diesel::{
	backend::Backend,
	deserialize::{self as diesel_de, FromSql},
	serialize::{self as diesel_ser, ToSql},
	sql_types::BigInt,
};
use std::{
	fmt,
	io::Write,
	ops::{Deref, DerefMut},
};

#[derive(Debug, Clone, Eq, PartialEq, Queryable, Insertable, Identifiable)]
#[table_name = "birthdays"]
#[primary_key(userid, guildid)]
pub struct Birthday {
	pub userid:   U64,
	pub guildid:  U64,
	pub birthday: DateTime<Utc>,
	pub tz:       i32,
}

#[derive(Debug, Clone, Eq, PartialEq, Queryable, Insertable, Identifiable)]
#[table_name = "awarded_roles"]
#[primary_key(userid, guildid)]
pub struct AwardedRole {
	pub userid:    U64,
	pub guildid:   U64,
	pub roleid:    U64,
	pub remove_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Eq, PartialEq, Insertable, Queryable, Identifiable)]
#[table_name = "guilds"]
#[primary_key(id)]
pub struct Guild {
	pub id:              U64,
	pub announce_in:     Option<U64>,
	pub announce_format: String,
	pub set_role:        Option<U64>,
	pub remove_role:     bool,
}

impl Guild {
	pub fn create(id: u64) -> Guild {
		Guild {
			id:              id.into(),
			announce_in:     None,
			announce_format: String::default(),
			set_role:        None,
			remove_role:     true,
		}
	}
}

/// Wrapper for u64 that can be used where diesel expects i64
#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq, Ord, PartialOrd, AsExpression, FromSqlRow)]
#[sql_type = "BigInt"]
pub struct U64(pub u64);

impl Deref for U64 {
	type Target = u64;
	fn deref(&self) -> &Self::Target { &self.0 }
}

impl DerefMut for U64 {
	fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl From<i64> for U64 {
	fn from(o: i64) -> Self { U64(o as u64) }
}

impl Into<i64> for U64 {
	fn into(self) -> i64 { self.0 as i64 }
}

impl From<u64> for U64 {
	fn from(o: u64) -> Self { U64(o) }
}

impl Into<u64> for U64 {
	fn into(self) -> u64 { self.0 }
}

impl fmt::Display for U64 {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result { write!(fmt, "{}", self.0) }
}

impl std::cmp::PartialEq<u64> for U64 {
	fn eq(&self, other: &u64) -> bool { self.0 == *other }
}

// As i64 can be losslessly converted to u64 and back,
// we simply transform the u64 to a i64 before handing it to diesel, and
// the same in the opposite direction (i64 -> u64)
//
// CAVEAT: Database queries will look a bit wierd / need manual conversion
// because a large u64 value may be stored as a negative i64 value in the
// database
impl<DB> ToSql<BigInt, DB> for U64
where
	DB: Backend,
	i64: ToSql<BigInt, DB>,
{
	fn to_sql<W: Write>(&self, out: &mut diesel_ser::Output<W, DB>) -> diesel_ser::Result {
		(**self as i64).to_sql(out)
	}
}

impl<DB> FromSql<BigInt, DB> for U64
where
	DB: Backend,
	i64: FromSql<BigInt, DB>,
{
	fn from_sql(bytes: Option<&DB::RawValue>) -> diesel_de::Result<Self> {
		Ok(i64::from_sql(bytes)?.into())
	}
}
