#[macro_use]
extern crate diesel; // bc there is no use diesel::macros::*;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
mod macros;

pub mod bot;
pub mod db;
pub mod utils;

embed_migrations!();

use crate::db::ConnectionPool;
use serenity::prelude::Mutex;
use std::{collections::HashSet, sync::Arc};
use tokio_diesel::AsyncRunQueryDsl;

macro_rules! maybe_fail {
	($e:expr, $rt:expr, $msg:expr) => {
		match $e {
			Ok(v) => v,
			Err(e) => {
				tracing::error!("failure: {}", $msg);
				tracing::error!("{}", e);
				return Err($rt);
				},
			}
	};
}

/// Bot Owner (heapunderflow)
pub const OWNER: u64 = 102_379_663_615_094_784;
/// Time that is considered "beginning of day"
///
/// (0, 1, 0) being 00:01:00 (00:01 AM)
pub const START_OF_DAY: (u32, u32, u32) = (0, 1, 0);

fn main() {
	let runtime = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.build()
		.expect("failed to create tokio runtime");

	match runtime.block_on(async { realmain().await }) {
		Ok(_) => (),
		Err(e) => std::process::exit(e),
	}
}

async fn realmain() -> Result<(), i32> {
	match kankyo::load(false) {
		Ok(_) => (),
		Err(e) => match e.kind() {
			std::io::ErrorKind::NotFound => (),
			_ => {
				eprintln!("error while loading .env file");
				return Err(1);
			},
		},
	};

	tracing_subscriber::fmt::init();

	let connection =
		maybe_fail!(db::create_connection(), 1, "Failed to establish connection to the database");

	let cns = Arc::clone(&connection);
	maybe_fail!(
		tokio::task::spawn_blocking(move || {
			match cns.get() {
				Ok(conn) => {
					maybe_fail!(embedded_migrations::run(&*conn), 1i32, "mig fail");
					Ok(())
				},
				Err(why) => {
					tracing::error!("failed to retrieve connection to database: {:?}", why);
					return Err(1);
				},
			}
		})
		.await
		.expect("task panicked"),
		1,
		"failed to run migrations!"
	);

	let token = maybe_fail!(
		kankyo::key("BOT_TOKEN").ok_or_else(|| "token_not_found"),
		1,
		"unable to find token in environment"
	);
	tracing::debug!("using token: {}...", &token[..10]);

	let mut gset = KnownGuilds::new();
	maybe_fail!(
		gset.populate(Arc::clone(&connection)).await,
		1,
		"failed to retrieve known guilds on startup"
	);

	let known_guilds = Arc::new(Mutex::new(gset));

	let bot = maybe_fail!(
		bot::Bot::new(&token, connection, known_guilds).await,
		1,
		"failed to create bot user"
	);

	let ctrlc = bot.register_ctrlc().await;

	if let Err(why) = bot.run_forever().await {
		tracing::error!("bot crashed: {:?}", why);
		return Err(2);
	}

	ctrlc.await.expect("Failed to gracefully shut down");

	Ok(())
}

#[derive(Debug)]
pub struct KnownGuilds {
	guilds: HashSet<u64>,
}

impl Default for KnownGuilds {
	fn default() -> Self { Self::new() }
}

impl KnownGuilds {
	pub fn new() -> Self { Self { guilds: Default::default() } }

	pub fn add(&mut self, g: u64) -> bool { self.guilds.insert(g) }
	pub fn remove(&mut self, g: u64) -> bool { self.guilds.remove(&g) }
	pub fn has(&self, g: u64) -> bool { self.guilds.contains(&g) }

	pub async fn populate(&mut self, pool: ConnectionPool) -> Result<(), tokio_diesel::AsyncError> {
		use crate::db::schema::guilds::dsl::*;
		let ex = guilds.load_async::<crate::db::models::Guild>(&*pool).await?;
		let mut pset = HashSet::with_capacity(ex.len());
		for l in ex {
			pset.insert(*l.id);
		}
		self.guilds = pset;

		Ok(())
	}

	pub fn read_iter<'a>(&'a self) -> impl std::iter::Iterator<Item = u64> + 'a {
		self.guilds.iter().map(|v| *v)
	}
}

impl serenity::prelude::TypeMapKey for KnownGuilds {
	type Value = Arc<Mutex<Self>>;
}

pub struct ReadySnd(pub Option<tokio::sync::oneshot::Sender<()>>);

impl ReadySnd {
	fn new() -> (ReadySnd, ReadyRecv) {
		let (otx, orx) = tokio::sync::oneshot::channel();
		(ReadySnd(Some(otx)), ReadyRecv(orx))
	}
}

impl serenity::prelude::TypeMapKey for ReadySnd {
	type Value = Self;
}

pub struct ReadyRecv(pub tokio::sync::oneshot::Receiver<()>);

impl serenity::prelude::TypeMapKey for ReadyRecv {
	type Value = Self;
}
