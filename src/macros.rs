#![allow(unused_macros)]

/// Behaves similar to the [`dbg`] macro, but instead uses [`trace`] instead of
/// stdout. Additionally it omits the expression in favor of file location info
macro_rules! SPAM_S {
	($e:expr) => {{
		#[allow(clippy::all)]
		match $e {
			tmp => {
				tracing::trace!(pos=?format!("{}#{}:{}", file!(), line!(), column!()), "{:?}", tmp);
				tmp
			},
			}
		}};

	($e:expr, $m:expr) => {{
		#[allow(clippy::all)]
		match $e {
			tmp => {
				tracing::trace!(pos=?format!("{}#{}:{}", file!(), line!(), column!()), "{{ {} }} = {:?}", $m, tmp);
				tmp
			},
			}
		}};

	(!pretty, $e:expr) => {{
		#[allow(clippy::all)]
		match $e {
			tmp => {
				let span = tracing::trace_span!("SPAM_S", pos=?format!("{}#{}:{}", file!(), line!(), column!()));
				let _guard = span.enter();

				let f = format!("{:#?}", tmp);
				for l in f.split('\n') {
					tracing::trace!("{}", l);
				}

				tmp
			},
			}
		}};

	(!pretty, $e:expr, $m:expr) => {{
		#[allow(clippy::all)]
		match $e {
			tmp => {
				let span = tracing::trace_span!("SPAM_S", pos=?format!("{}#{}:{}", file!(), line!(), column!()), msg=?$m);
				let _guard = span.enter();

				let f = format!("{:#?}", tmp);
				for l in f.split('\n') {
					tracing::trace!("{}", l);
				}

				tmp
			},
			}
		}};
}

/// Log an [`error`] if the given expression returns an Err
macro_rules! err_log {
	($e:expr) => {
		if let Err(why) = $e {
			tracing::error!(
				"[{}#{}:{}] {{ {} }} = {:?}",
				file!(),
				line!(),
				column!(),
				stringify!($e),
				why
				);
			}
	};
}
