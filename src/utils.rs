use chrono::{Datelike, FixedOffset, NaiveDate, Utc};
use regex::Regex;
use snafu::OptionExt;
use std::fmt::Write;

/// Clamp v between l and r (inclusivly)
#[inline]
pub fn clamp2<T>(v: T, l: T, r: T) -> T
where
	T: PartialOrd,
{
	if v < l {
		l
	} else if v > r {
		r
	} else {
		v
	}
}

/// Extension method for Vec<T>
pub trait VecExt<T> {
	fn maybe_swap_remove<P>(&mut self, predicate: P) -> Option<T>
	where
		P: Fn(&T) -> bool;
}

impl<T> VecExt<T> for Vec<T> {
	/// Finds the first element that matches `predicate` and then uses
	/// swap_remove to return remove it from the list. returns the removed
	/// element (if any)
	///
	/// This does not preserve ordering.
	fn maybe_swap_remove<P>(&mut self, predicate: P) -> Option<T>
	where
		P: Fn(&T) -> bool,
	{
		for i in 0..self.len() {
			if predicate(&self[i]) {
				let r = self.swap_remove(i);
				return Some(r);
			}
		}
		None
	}
}

/// Convert either a mention or a raw id to a u64
pub fn mention_or_id(s: &str) -> Option<u64> {
	lazy_static::lazy_static! {
		static ref R: Regex = Regex::new(r"(?:<[^\d]{1,2}(\d+)>)|(?:\b(\d+)\b)").expect("invalid regex (id)");
	}

	let cap = R.captures(&s)?;

	Some(cap.get(1).or_else(|| cap.get(2))?.as_str().parse::<u64>().ok()?)
}

/// Error for parsing a date out of a string
pub mod parse_date_error {
	use snafu::Snafu;

	#[derive(Snafu, Debug)]
	#[snafu(visibility = "pub(super)")]
	pub enum ParseDateError {
		InvalidFormat,
		OutOfRange,
		NaN,
	}
}

use parse_date_error::ParseDateError;

pub fn parse_date(s: &str) -> Result<NaiveDate, ParseDateError> {
	lazy_static::lazy_static! {
		static ref R: Regex = Regex::new(r"(\d{1,2})[-\\/_\|](\d{1,2})").expect("invalid regex (date)");
	}

	let cap = R.captures(&s).context(parse_date_error::InvalidFormat)?;

	let month = cap.get(1).unwrap().as_str().parse().ok().context(parse_date_error::NaN)?;
	let day = cap.get(2).unwrap().as_str().parse().ok().context(parse_date_error::NaN)?;

	let today = Utc::today();
	let mut dt =
		NaiveDate::from_ymd_opt(today.year(), month, day).context(parse_date_error::OutOfRange)?;

	if dt < today.naive_utc() {
		dt = NaiveDate::from_ymd(dt.year() + 1, dt.month(), dt.day());
	}
	Ok(dt)
}

/// Parse and clamp a timezone
pub fn parse_timezone(s: &str) -> Option<(i32, FixedOffset)> {
	let parts = s.splitn(2, ':').collect::<Vec<_>>();
	let minutes_east: i32 = match &parts[..] {
		[hh] => {
			let h = clamp2(hh.parse::<i32>().ok()?, -12, 13);
			// we only got the hours
			h * 60
		},
		[hh, mm] => {
			let h = clamp2(hh.parse::<i32>().ok()?, -12, 13);
			let m = clamp2(mm.parse::<i32>().ok()?, 0, 59);
			// we got both hours and minutes
			h * 60 + (if h < 0 { m * -1 } else { m })
		},
		_ => unreachable!("the pattern should never be longer than 2"),
	};

	Some((minutes_east, FixedOffset::east_opt(minutes_east * 60)?))
}

/// Adapted from chrono's Debug implementation of FixedOffset
pub fn stringify_offset(f: FixedOffset) -> String {
	let offset = f.local_minus_utc();
	let (sign, offset) = if offset < 0 { ('-', -offset) } else { ('+', offset) };
	let (mins, _) = div_mod_floor(offset, 60);
	let (hour, min) = div_mod_floor(mins, 60);
	format!("UTC{}{:02}:{:02}", sign, hour, min)
}

/// Calculates `div_floor` and `mod_floor` simultaneously
/// Taken (stolen) from [num_integer](https://github.com/rust-num/num-integer)
#[inline]
pub fn div_mod_floor(left: i32, right: i32) -> (i32, i32) {
	// Algorithm from [Daan Leijen. _Division and Modulus for Computer Scientists_,
	// December 2001](http://research.microsoft.com/pubs/151917/divmodnote-letter.pdf)
	let (d, r) = div_rem(left, right);
	if (r > 0 && right < 0) || (r < 0 && right > 0) {
		(d - 1, r + right)
	} else {
		(d, r)
	}
}

/// Simultaneous truncated integer division and modulus.
/// Taken (stolen) from [num_integer](https://github.com/rust-num/num-integer)
#[inline]
pub fn div_rem(left: i32, right: i32) -> (i32, i32) { (left / right, left % right) }

// Adapted from https://crates.io/humantime 's format_duration
// we are using this instead of humantimes for 2 reasons:
// - Reduced dependencies (even tho that shouldnt matter in the face of
//   humantime)
// - Humantime has some wierd behavior with hours and lower, which prompted me
//   to create this it is functionally equivalent without hours, minutes and
//   seconds

/// Format a duration in a human readanble format
pub trait HumanDuration {
	/// Format a duration in a human readanble format
	fn human_format(&self) -> String;

	/// Format a duration in a human readable format, turning on/off options as
	/// needed
	fn human_format_opt(
		&self,
		years: bool,
		months: bool,
		days: bool,
		hours: bool,
		minutes: bool,
		seconds: bool,
	) -> String;
}

impl HumanDuration for std::time::Duration {
	fn human_format(&self) -> String {
		human_format_duration(self.as_secs(), true, true, true, false, false, false)
	}
	fn human_format_opt(
		&self,
		years: bool,
		months: bool,
		days: bool,
		hours: bool,
		minutes: bool,
		seconds: bool,
	) -> String {
		human_format_duration(self.as_secs(), years, months, days, hours, minutes, seconds)
	}
}

impl HumanDuration for chrono::Duration {
	fn human_format(&self) -> String {
		human_format_duration(self.num_seconds() as u64, true, true, true, false, false, false)
	}
	fn human_format_opt(
		&self,
		years: bool,
		months: bool,
		days: bool,
		hours: bool,
		minutes: bool,
		seconds: bool,
	) -> String {
		human_format_duration(
			self.num_seconds() as u64,
			years,
			months,
			days,
			hours,
			minutes,
			seconds,
		)
	}
}

fn human_format_duration(
	secs: u64,
	p_years: bool,
	p_months: bool,
	p_days: bool,
	p_hours: bool,
	p_minutes: bool,
	p_seconds: bool,
) -> String {
	if secs == 0 {
		return String::from("0days");
	}

	let mut out = String::default();

	let years = secs / 31_557_600; // 365.25d
	let ydays = secs % 31_557_600;
	let months = ydays / 2_630_016; // 30.44d
	let mdays = ydays % 2_630_016;
	let days = mdays / 86_400;
	let day_secs = mdays % 86_400;
	let hours = day_secs / 3_600;
	let minutes = day_secs % 3_600 / 60;
	let seconds = day_secs % 60;

	let mut started = false;
	if p_years {
		item_plural(&mut out, &mut started, "year", years);
	}
	if p_months {
		item_plural(&mut out, &mut started, "month", months);
	}
	if p_days {
		item_plural(&mut out, &mut started, "day", days);
	}
	if p_hours {
		item(&mut out, &mut started, "h", hours as u32);
	}
	if p_minutes {
		item(&mut out, &mut started, "m", minutes as u32);
	}
	if p_seconds {
		item(&mut out, &mut started, "s", seconds as u32);
	}

	out
}

fn item_plural(f: &mut String, started: &mut bool, name: &str, value: u64) {
	if value > 0 {
		if *started {
			let _ = f.write_str(" ");
		}
		let _ = f.write_str(&format!("{}{}", value, name));
		if value > 1 {
			let _ = f.write_str("s");
		}
		*started = true;
	}
}

fn item(f: &mut String, started: &mut bool, name: &str, value: u32) {
	if value > 0 {
		if *started {
			let _ = f.write_str(" ");
		}
		let _ = write!(f, "{}{}", value, name);
		*started = true;
	}
}
