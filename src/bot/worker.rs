use crate::db::{
	models::{Birthday, Guild, U64},
	ConnectionPool,
};
use chrono::{Datelike, Utc};
use diesel::prelude::*;
use futures::stream::StreamExt;
use serenity::{
	model::id::{ChannelId, GuildId},
	CacheAndHttp as CAH,
};
use std::sync::Arc;
use tokio::{sync::Mutex, task::JoinHandle, time::Duration};
use tokio_diesel::AsyncRunQueryDsl;
use tracing_futures::Instrument;

pub struct Worker {
	#[allow(dead_code)]
	jh: JoinHandle<()>,
}

impl Worker {
	pub fn start(
		dbc: ConnectionPool,
		cah: Arc<CAH>,
		known_guilds: Arc<Mutex<crate::KnownGuilds>>,
		ready: crate::ReadyRecv,
	) -> Self {
		Self {
			jh: tokio::task::spawn(async move {
				Self::run_fn(dbc, cah, known_guilds, ready)
					.instrument(tracing::debug_span!("worker"))
					.await
			}),
		}
	}

	async fn run_fn(
		db: ConnectionPool,
		cah: Arc<CAH>,
		known_guilds: Arc<Mutex<crate::KnownGuilds>>,
		ready: crate::ReadyRecv,
	) {
		// wait for ready
		let _ = ready.0.await;
		tracing::debug!("ready recieved, lets go!");
		tracing::debug!("cleaning up all birthdays we had lost between restarts");
		match crate::db::schema::birthdays::table.load_async::<Birthday>(&*db).await {
			Ok(mut birthdays) => {
				for guild in known_guilds.lock().await.read_iter() {
					let mut member_stream = GuildId(guild)
						.members_iter(&cah.http)
						.filter_map(|el| async move { el.ok() })
						.boxed();
					while let Some(member) = member_stream.next().await {
						let idx = birthdays
							.iter()
							.enumerate()
							.find(|(_, el)| el.userid == member.user.id.0 && el.guildid == guild)
							.map(|(idx, _)| idx);
						if let Some(idx) = idx {
							birthdays.swap_remove(idx);
						}
					}
				}

				let idmp: Vec<(u64, u64)> =
					birthdays.into_iter().map(|bd| (*bd.guildid, *bd.userid)).collect();

				// TODO: Use postgres batch transactions here
				let mut count = 0;
				for (g, u) in &idmp {
					use crate::db::schema::birthdays::dsl::*;
					use diesel::prelude::*;
					tracing::debug!(%g, %u, "removing birthday of invalid user");
					match diesel::delete(
						birthdays.filter(guildid.eq(U64(*g)).and(userid.eq(U64(*u)))),
					)
					.execute_async(&*db)
					.await
					{
						Ok(c) => count += c,
						Err(why) => {
							tracing::warn!("failed to remove birthday: {:?}", why);
						},
					}
				}

				tracing::info!(had=%idmp.len(), removed=%count, "invalid user bday removal");
			},
			Err(why) => {
				tracing::error!("failed to retrieve birthdays from db, skipping: {:?}", why);
			},
		}

		// loop
		loop {
			let now = Utc::now();
			let loop_span = tracing::trace_span!("loop");
			Self::process_removals(now, Arc::clone(&db), Arc::clone(&cah))
				.instrument(loop_span.clone())
				.await;
			Self::process_additions(now, Arc::clone(&db), Arc::clone(&cah))
				.instrument(loop_span.clone())
				.await;
			tracing::trace!(parent: loop_span, "loop complete");
			tokio::time::sleep(Duration::from_secs(30)).await;
		}
	}

	// marked as inline as it *should* only be used *once* ever
	#[inline]
	#[tracing::instrument(skip(now, db, cah))]
	async fn process_removals(now: chrono::DateTime<Utc>, db: ConnectionPool, cah: Arc<CAH>) {
		// role removals
		use crate::db::schema::awarded_roles::dsl::*;
		match awarded_roles
			.filter(remove_at.lt(now))
			.get_results_async::<crate::db::models::AwardedRole>(&*db)
			.await
		{
			Err(e) => {
				tracing::error!("failed to retrieve awarded_roles list from db: {:?}", e);
			},
			Ok(data) => {
				let mut oaw = Vec::new();
				for aw in data {
					let mut member = match GuildId(*aw.guildid)
						.member(Arc::clone(&cah), *aw.userid)
						.await
					{
						Ok(m) => m,
						Err(e) => {
							tracing::error!(
								"unable to retieve member {} for guild {} from http (E={:?})",
								aw.userid,
								aw.guildid,
								e
							);
							continue;
						},
					};

					match member.remove_role(&cah.http, *aw.roleid).await {
						Ok(_) => {
							tracing::debug!(?aw.userid, ?aw.roleid, ?aw.guildid, "handled autorem")
						},
						Err(e) => {
							tracing::error!(?aw.userid, ?aw.roleid, ?aw.guildid, "failed autorem: {:?}", e);
						},
					}

					oaw.push(aw);
				}
				let oawc = oaw.len();
				let mut owc: usize = 0;

				for ow in oaw {
					let cc = db.clone();
					match tokio::task::spawn_blocking(move || {
						let conn = cc.get().map_err(tokio_diesel::AsyncError::Checkout)?;
						diesel::delete(
							awarded_roles.filter(guildid.eq(ow.guildid).and(userid.eq(ow.userid))),
						)
						.execute(&*conn)
						.map_err(tokio_diesel::AsyncError::Error)
					})
					.await
					.expect("task panicked")
					{
						Ok(v) => {
							tracing::trace!(current=%owc+1, total=%oawc, "cleared up role");
							owc += v;
						},
						Err(e) => tracing::error!("failed to clear up aw roles: {:?}", e),
					}
				}
			},
		}
	}

	// marked as inline as it *should* only be used *once* ever
	#[inline]
	#[tracing::instrument(skip(now, db, cah))]
	async fn process_additions(now: chrono::DateTime<Utc>, db: ConnectionPool, cah: Arc<CAH>) {
		use crate::db::schema::{awarded_roles, birthdays, guilds};
		let data = match birthdays::table
			.filter(birthdays::birthday.lt(now))
			.inner_join(guilds::table)
			.get_results_async::<(Birthday, Guild)>(&*db)
			.await
		{
			Err(e) => {
				tracing::error!("failed to retrieve the upcoming bday list: {:?}", e);
				return;
			},
			Ok(data) => data,
		};

		let mut naw = Vec::new();
		for (bday, gld) in data {
			Self::convert_birthday(now, bday, gld, Arc::clone(&cah), &mut naw, Arc::clone(&db))
				.await;
		}
		if !naw.is_empty() {
			if let Err(e) =
				diesel::insert_into(awarded_roles::table).values(naw).execute_async(&*db).await
			{
				tracing::warn!("unable to insert awarded roles: {:?}", e);
			}
		}
	}

	// marked as inline as it *should* only be used *once* ever
	#[inline]
	#[tracing::instrument(skip(now, bday, gld, cah, naw, db))]
	async fn convert_birthday(
		now: chrono::DateTime<Utc>,
		bday: Birthday,
		gld: Guild,
		cah: Arc<CAH>,
		naw: &mut Vec<crate::db::models::AwardedRole>,
		db: ConnectionPool,
	) {
		use crate::db::schema::birthdays;
		let member = match GuildId(*bday.guildid).member(&cah, *bday.userid).await
		{
			Ok(m) => Some(m),
			Err(e) => {
				tracing::error!(?bday.userid, ?bday.guildid, "member fetch error: {:?}", e);
				None
			},
		};

		if let Some(announce_in) = gld.announce_in {
			let mut message = gld
				.announce_format
				.replace("{{mention}}", &format!("<@{}>", *bday.userid))
				.replace("{{id}}", &bday.userid.to_string());
			if let Some(roleid) = gld.set_role {
				message = message.replace("{{role}}", &format!("<@&{}>", roleid));
			} else {
				message = message.replace("{{role}}", "");
			};

			if let Some(ref name) = member {
				message = message.replace("{{name}}", &name.user.name);
			} else {
				message = message.replace("{{name}}", "[No User]");
			}

			err_log!(ChannelId(*announce_in).say(&cah.http, message).await);
			tracing::info!(?bday.userid, ?announce_in, bday.guildid=?gld.id, "announced birthday");
		}
		if let Some(roleid) = gld.set_role {
			if let Some(mut mem) = member {
				match mem.add_role(&cah.http, *roleid).await {
					Ok(_) => {
						tracing::info!(role.id=?roleid, ?bday.userid, ?bday.guildid, "granted role");
					},
					Err(e) => tracing::warn!(
						role.id=?roleid, ?bday.userid, ?bday.guildid, "role grant error: {:?}", e
					),
				}

				naw.push(crate::db::models::AwardedRole {
					userid: bday.userid,
					guildid: bday.guildid,
					roleid,
					remove_at: now + chrono::Duration::days(1),
				});
			} else {
				tracing::warn!("could not award role: member doesnt exist");
			}
		}

		let nbd = match bday.birthday.with_year(bday.birthday.year() + 1) {
			Some(v) => v,
			None => {
				tracing::error!("what the fuck is this year");
				return;
			},
		};

		match tokio::task::spawn_blocking(move || {
			let conn = db.get().map_err(tokio_diesel::AsyncError::Checkout)?;
			diesel::update(birthdays::table.find(bday.id()))
				.set(birthdays::dsl::birthday.eq(nbd))
				.execute(&conn)
				.map_err(tokio_diesel::AsyncError::Error)
		})
		.await
		{
			Ok(_) => (),
			Err(e) => {
				tracing::error!("failed to update birthday: {:?}", e);
			},
		}
	}
}
