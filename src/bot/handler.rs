use crate::db::models::U64;
use serenity::{
	framework::standard::{
		help_commands,
		macros::help,
		Args,
		CommandGroup,
		CommandResult,
		HelpOptions,
	},
	model::{
		channel::Message,
		guild::{Guild, Member},
		id::{GuildId, UserId},
		user::User,
	},
	prelude::*,
};
use std::{collections::HashSet, sync::Arc};
use tokio_diesel::{AsyncError, AsyncRunQueryDsl};

#[derive(Debug)]
pub struct Handler;

#[serenity::async_trait]
impl EventHandler for Handler {
	#[tracing::instrument(skip(ctx, gd))]
	async fn guild_create(&self, ctx: Context, gd: Guild, _: bool) {
		use crate::db::schema::guilds::dsl::*;
		use diesel::prelude::*;
		// -<[#|-|#]>

		let (db, kguilds) = {
			let _ctx = ctx.data.read().await;
			let _db =
				Arc::clone(_ctx.get::<super::DbConnection>().expect("failed to find db in ctx"));
			let _kg =
				Arc::clone(_ctx.get::<crate::KnownGuilds>().expect("failed to find known guilds"));
			(_db, _kg)
		};

		let mut kgl = kguilds.lock().await;
		if !kgl.has(gd.id.0) {
			let new_gld = crate::db::models::Guild::create(gd.id.0);
			match tokio::task::spawn_blocking(move || {
				let conn = db.get().map_err(AsyncError::Checkout)?;
				diesel::insert_into(guilds)
					.values(&new_gld)
					.execute(&*conn)
					.map_err(AsyncError::Error)
			})
			.await
			{
				Ok(_) => {
					tracing::info!("inserted new guild: {}", gd.id.0);
					let _ = kgl.add(gd.id.0);
				},
				Err(e) => {
					tracing::warn!("failed to insert new guild, may need to call init: e {:?}", e);
				},
			}
		}

		{
			let mut adata = ctx.data.write().await;
			if let Some(crd) = adata.get_mut::<crate::ReadySnd>() {
				if crd.0.is_some() {
					let _ = std::mem::take(&mut crd.0).unwrap().send(());
				}
			}
		}
	}

	#[tracing::instrument(skip(ctx, user))]
	async fn guild_member_removal(
		&self,
		ctx: Context,
		guild: GuildId,
		user: User,
		_: Option<Member>,
	) {
		use crate::db::schema::birthdays::dsl::*;
		use diesel::prelude::*;

		tracing::debug!(?user.id);

		let db = Arc::clone(
			&ctx.data.read().await.get::<super::DbConnection>().expect("failed to find db in ctx"),
		);
		let res = match diesel::delete(
			birthdays.filter(guildid.eq(U64(guild.0)).and(userid.eq(U64(user.id.0)))),
		)
		.execute_async(&*db)
		.await
		{
			Ok(res) => res,
			Err(error) => {
				tracing::error!("failed to delete row: {:?}", error);
				return;
			},
		};

		if res == 1 {
			tracing::info!(?user.id, "user left guild, removed birthday");
		} else if res == 0 {
			tracing::info!(?user.id, "user left guild, but had no birthday. ignoring");
		} else if res > 1 {
			tracing::warn!(
				"deleted count is bigger than 1, something is wrong with the primary key \
				 constraint!"
			);
		}
	}
}

#[help]
#[command_not_found_text = "Unknown: '{}'"]
#[max_levenshtein_distance(3)]
#[lacking_permissions = "Hide"]
#[wrong_channel = "Strike"]
#[lacking_role = "Strike"]
#[indention_prefix = "+"]
async fn help_command(
	context: &Context,
	msg: &Message,
	args: Args,
	help_options: &'static HelpOptions,
	groups: &[&'static CommandGroup],
	owners: HashSet<UserId>,
) -> CommandResult {
	let _ = help_commands::plain(context, msg, args, help_options, groups, owners).await;
	Ok(())
}
