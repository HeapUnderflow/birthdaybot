use crate::{
	bot::DbConnection,
	db::{
		models::{Birthday, U64},
		BirthdayError,
	},
	utils::HumanDuration,
};
use chrono::Utc;
use debug::DEBUG_GROUP;
use futures::stream::StreamExt as _;
use serenity::{
	framework::standard::{
		macros::{command, group},
		Args,
		CommandResult,
	},
	model::channel::Message,
	prelude::*,
};
use set::SET_GROUP;
use std::sync::Arc;
use tokio_diesel::AsyncRunQueryDsl;
use unicode_width::UnicodeWidthStr;

mod debug;
mod set;

#[group]
// #[checks()]
#[commands(about, upcoming, add, rm)]
#[sub_groups(set, debug)]
pub struct General;

/// Retrieve info about the bot
#[tracing::instrument(skip(ctx, msg))]
#[command]
#[description = "infos about the bot"]
async fn about(ctx: &Context, msg: &Message) -> CommandResult {
	err_log!(
		msg.channel_id
			.say(
				&ctx.http,
				concat!(
					"**About**:\nBirthdayBot v",
					env!("CARGO_PKG_VERSION"),
					"\n",
					include_str!("bot/commands/about_text.txt"),
					"\n\nBuild Info:\n",
					include_str!(concat!(env!("OUT_DIR"), "/discord_friendly_build_stats.txt"))
				)
			)
			.await
	);
	Ok(())
}

/// Show upcoming birthdays
#[tracing::instrument(skip(ctx, msg, args))]
#[command]
#[description = "see the upcoming birthdays for this server"]
#[usage = "[count]"]
#[only_in("guilds")]
async fn upcoming(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
	let db = Arc::clone(
		ctx.data.read().await.get::<DbConnection>().expect("db connection was never inserted"),
	);

	let guild_id = match msg.guild_id {
		Some(v) => v,
		None => {
			tracing::debug!("not a guild");
			return Err("not a guild".into());
		},
	};

	let show_num_birthdays = match args.single::<usize>() {
		Ok(v) if 1 <= v && v <= 25 => v,
		Ok(v) if v == 0 => 1,
		Ok(v) if v >= 26 => 25,
		Ok(v) => unreachable!("the {} option was not covered", v),
		Err(_) => 5,
	};

	let mut upcoming_unresolved: Vec<_> = match crate::db::get_upcoming_birthdays(
		Arc::clone(&db),
		guild_id.0,
		show_num_birthdays as i64,
	)
	.await
	{
		Ok(v) => v,
		Err(e) => {
			tracing::error!("error requesting birthdays from the database: {:?}", e);
			err_log!(
				msg.channel_id
					.say(
						&ctx.http,
						"Unable to request birthdays from the database.\nPlease contact \
						 HeapUnderflow (command: about) about this (they probably fucked up)"
					)
					.await
			);
			return Err(format!("error requesting birthdays from the database: {:?}", e).into());
		},
	};

	let mut member_stream = guild_id.members_iter(&ctx.http).boxed();

	let mut upcoming: Vec<(Birthday, String)> = Vec::new();

	while let Some(member) = member_stream.next().await {
		if upcoming_unresolved.len() == 0 {
			// Break if we have nothing to resolve
			break;
		}
		let member = match member {
			Ok(member) => member,
			Err(why) => {
				tracing::warn!("failed to resolve member: {:?}", why);
				continue;
			},
		};

		if let Some(idx) = upcoming_unresolved
			.iter()
			.enumerate()
			.find(|(_, bdel)| member.user.id.0 == *bdel.userid)
			.map(|(idx, _)| idx)
		{
			let bd = upcoming_unresolved.swap_remove(idx);
			tracing::trace!(id=%bd.userid, name=?member.user.name, "resolved user name");
			upcoming.push((bd, member.user.name.clone()));
		}
	}

	upcoming.sort_unstable_by(|(l, _), (r, _)| l.birthday.cmp(&r.birthday));

	let mut out = String::new();
	out.push_str("Upcoming Birthdays:\n```\n");

	let now = Utc::today();
	let max_width = upcoming.iter().fold(0usize, |current, elem| {
		let len = UnicodeWidthStr::width(elem.1.as_str());
		if current < len {
			len
		} else {
			current
		}
	});

	for (bd, nm) in upcoming {
		let actual = bd.birthday.with_timezone(&chrono::FixedOffset::east(bd.tz));
		out.push_str(&format!(
			"{:name_width$} ({:id_width$}) on {} [in {}]\n",
			nm,
			bd.userid,
			actual.format("%Y-%m-%d%z"),
			actual
				.date()
				.signed_duration_since(now)
				.human_format_opt(true, true, true, true, false, false),
			name_width = max_width,
			id_width = 18
		));
	}

	out.push_str("```");

	if out.len() >= 2000 {
		err_log!(msg.channel_id.say(&ctx.http, "character limit reached (too many users)").await);
	} else {
		err_log!(msg.channel_id.say(&ctx.http, &out).await);
	}

	Ok(())
}

/// Add a new birthday to the database
#[tracing::instrument(skip(ctx, msg, args))]
#[command("add")]
#[aliases("create")]
#[description = "add a birthday"]
#[usage = "<user|id> <date> [timezone]"]
#[example = "231215132121 06/22 -4"]
#[example = "231215132121 06/22 5:30"]
#[min_args(2)]
#[max_args(3)]
#[only_in("guilds")]
async fn add(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
	let db = Arc::clone(
		ctx.data.read().await.get::<DbConnection>().expect("db connection was never inserted"),
	);

	let guild_id = match msg.guild_id {
		Some(v) => v,
		None => {
			tracing::debug!("not a guild");
			return Err("not a guild".into());
		},
	};

	let user_id = match args.current() {
		Some(v) => match crate::utils::mention_or_id(&v) {
			Some(id) => id,
			None => {
				err_log!(msg.channel_id.say(&ctx.http, "Invalid user (id)").await);
				return Err("invalud user id".into());
			},
		},
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "Missing user").await);
			return Err("missing user".into());
		},
	};

	if msg.author.id.0 != user_id && msg.author.id.0 != crate::OWNER {
		if let Ok(member) = msg.member(&ctx).await {
			if let Ok(permissions) = member.permissions(&ctx).await {
				if !permissions.administrator() || !permissions.manage_roles() {
					SPAM_S!("no permissions for guild");
					return Ok(());
				}
			} else {
				err_log!(msg.channel_id.say(&ctx.http, "unable to verify permissions").await);
				return Err("unable to verify permissions (perms)".into());
			}
		} else {
			err_log!(msg.channel_id.say(&ctx.http, "unable to verify permissions").await);
			return Err("unable to verify permissions (member)".into());
		}
	}

	args.advance();

	let date = match args.current() {
		Some(v) => {
			use crate::utils::{parse_date, parse_date_error as pde};
			match parse_date(v) {
				Ok(dt) => dt,
				Err(e) => {
					let r = match e {
						pde::ParseDateError::OutOfRange => "the provided date is out of range",
						_ => "the provided date has a invalid format",
					};
					err_log!(msg.channel_id.say(&ctx.http, r).await);
					return Err(r.into());
				},
			}
		},
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "Missing date").await);
			return Err("missing date".into());
		},
	};

	args.advance();

	let (tzo, timezone) = match args.current() {
		Some(v) => match crate::utils::parse_timezone(v) {
			Some(tz) => tz,
			None => {
				err_log!(
					msg.channel_id
						.say(&ctx.http, "could not parse timezone, is it a number?")
						.await
				);
				return Err("invalid tz".into());
			},
		},
		None => (0, chrono::FixedOffset::east(0)),
	};

	match crate::db::add_birthday(Arc::clone(&db), guild_id.0, user_id, date, timezone).await {
		Ok(()) => {
			let r = format!(
				"added <@{}>'s birthday for {} (with timezone {})",
				user_id,
				date.format("%b %e"),
				crate::utils::stringify_offset(timezone)
			);
			err_log!(msg.channel_id.say(&ctx.http, &r).await);
			tracing::info!("{}", r);
		},
		Err(e) => match e {
			BirthdayError::WhatTheFuckIsThisDate => {
				err_log!(
					msg.channel_id
						.say(
							&ctx.http,
							"Unable to properly construct a date. congratulations. you broke it."
						)
						.await
				);
				return Err("unable to construct date".into());
			},
			BirthdayError::AlreadyExists => {
				err_log!(
					msg.channel_id
						.say(&ctx.http, "you can only have 1 birthday per server, duh.")
						.await
				);
				return Err(
					format!("more than 1 birthday ({} => {} + tz={})", user_id, date, tzo).into()
				);
			},
			BirthdayError::InconsistentInsert => {
				err_log!(
					msg.channel_id
						.say(
							&ctx.http,
							"Unable to save, please tell heap about this (command: about)"
						)
						.await
				);
				return Err("inserted 0 rows".into());
			},
			BirthdayError::NoGuildFound { gid } => {
				err_log!(
					msg.channel_id
						.say(
							&ctx.http,
							"for some reason i dont have any data for this guild. please contact \
							 heap for more info (command: about)"
						)
						.await
				);
				return Err(format!("missing guild (gid={})", gid).into());
			},
			BirthdayError::Other { source } => {
				err_log!(
					msg.channel_id
						.say(
							&ctx.http,
							"ok. something fatally broke. please contact heap with the code \
							 `ok_fuck_1`"
						)
						.await
				);
				return Err(format!("[[ok_fuck_1]] {:?}", source).into());
			},
			_ => (),
		},
	}

	Ok(())
}

#[tracing::instrument(skip(ctx, msg, args))]
#[command]
#[aliases("remove", "delete")]
#[description = "remove a birthday"]
#[usage = "<user|id>"]
#[example = "123214135123"]
#[min_args(1)]
#[max_args(1)]
#[only_in("guilds")]
async fn rm(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
	let db = Arc::clone(
		ctx.data.read().await.get::<DbConnection>().expect("db connection was never inserted"),
	);

	let guild_id = match msg.guild_id {
		Some(v) => v,
		None => {
			tracing::debug!("not a guild");
			return Err("not a guild".into());
		},
	};

	let user_id = match args.current() {
		Some(v) => match crate::utils::mention_or_id(&v) {
			Some(id) => id,
			None => {
				err_log!(msg.channel_id.say(&ctx.http, "Invalid user (id)").await);
				return Err("invalud user id".into());
			},
		},
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "Missing user").await);
			return Err("missing user".into());
		},
	};

	if msg.author.id.0 != user_id {
		if let Ok(member) = msg.member(&ctx).await {
			if let Ok(permissions) = member.permissions(&ctx).await {
				if !permissions.administrator() || !permissions.manage_roles() {
					SPAM_S!("no permissions for guild");
					return Ok(());
				}
			} else {
				err_log!(msg.channel_id.say(&ctx.http, "unable to verify permissions").await);
				return Err("unable to verify permissions (perms)".into());
			}
		} else {
			err_log!(msg.channel_id.say(&ctx.http, "unable to verify permissions").await);
			return Err("unable to verify permissions (member)".into());
		}
	}

	let bd = {
		use crate::db::schema::birthdays::dsl::*;
		use diesel::prelude::*;

		let bdays = match birthdays
			.filter(guildid.eq(U64::from(guild_id.0)).and(userid.eq(U64::from(user_id))))
			.load_async::<crate::db::models::Birthday>(&*db)
			.await
		{
			Ok(v) => v,
			Err(e) => {
				return Err(format!("failed to find birthday data: {:?}", e).into());
			},
		};

		match bdays.into_iter().next() {
			Some(v) => v,
			None => {
				err_log!(msg.channel_id.say(&ctx.http, "oof").await);
				return Err("failed to find birthday".into());
			},
		}
	};

	match crate::db::remove_birthday(Arc::clone(&db), bd.clone()).await {
		Ok(_) => {
			err_log!(
				msg.channel_id
					.say(
						&ctx.http,
						&format!(
							"removed <@{}>'s birthday on {}",
							bd.userid,
							bd.birthday
								.with_timezone(&chrono::FixedOffset::east(bd.tz))
								.format("%b %e"),
						)
					)
					.await
			);

			tracing::info!("removed birthday {} for {} in {}", bd.birthday, bd.userid, bd.guildid);
			Ok(())
		},
		Err(e) => {
			err_log!(msg.channel_id.say(&ctx.http, "failed to remove birthday").await);

			let fmt = format!(
				"failed to remove birthday {} for {} in {} because of {:?}",
				bd.birthday, bd.userid, bd.guildid, e
			);
			tracing::debug!("{}", &fmt);
			Err(fmt.into())
		},
	}
}
