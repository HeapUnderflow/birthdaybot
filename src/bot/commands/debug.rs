use super::super::checks::OWNER_CHECK;
use crate::bot::DbConnection;
use serenity::{
	framework::standard::{
		macros::{command, group},
		Args,
		CommandResult,
	},
	model::channel::Message,
	prelude::*,
};
use std::sync::Arc;
use tokio_diesel::AsyncError;

#[group]
#[checks(Owner)]
#[prefix("debug")]
#[commands(init_guild)]
pub struct Debug;

#[tracing::instrument(skip(ctx, msg, args))]
#[command]
#[description = "initialize guild data"]
async fn init_guild(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
	let db = Arc::clone(
		ctx.data.read().await.get::<DbConnection>().expect("db connection was never inserted"),
	);

	let guild_id = match args.single::<u64>() {
		Ok(gid) => serenity::model::id::GuildId(gid),
		Err(_) => match msg.guild_id {
			Some(v) => v,
			None => {
				tracing::debug!("not a guild");
				return Err("not a guild".into());
			},
		},
	};

	let newg = crate::db::models::Guild::create(guild_id.0);

	{
		use crate::db::schema::guilds::dsl::*;
		use diesel::prelude::*;

		let res = tokio::task::spawn_blocking(move || {
			let conn = db.get().map_err(AsyncError::Checkout)?;
			diesel::insert_into(guilds).values(&newg).execute(&*conn).map_err(AsyncError::Error)
		})
		.await;
		err_log!(match res {
			Ok(y) => {
				ctx.data
					.read() // locks
					.await
					.get::<crate::KnownGuilds>()
					.expect("missing known guilds")
					.lock() // locks
					.await
					.add(guild_id.0);
				msg.channel_id.say(&ctx.http, format!("yup {:?}", y)).await
			},
			Err(e) => msg.channel_id.say(&ctx.http, format!("nop {:?}", e)).await,
		});
	}

	Ok(())
}
