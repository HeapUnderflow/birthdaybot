use crate::{
	bot::checks::{ADMIN_CHECK, OWNER_CHECK},
	db::models::U64,
};
use serenity::{
	framework::standard::{
		macros::{command, group},
		Args,
		CommandResult,
	},
	model::channel::Message,
	prelude::*,
};
use std::sync::Arc;
use tokio_diesel::{AsyncError, AsyncRunQueryDsl};

#[group]
#[checks(Admin)]
#[prefix("set")]
#[only_in("guilds")]
#[commands(set_format, set_channel, set_role)]
pub struct Set;

#[tracing::instrument(skip(ctx, msg, args))]
#[command("format")]
#[description(
	r##"configure the birthday message
avaliable format replacements:
- `{{mention}}` a mention the birthday user (eg `@HelloWorld#1234`)
- `{{id}}` the id of the birthday user (eg `11223344556677`)
- `{{name}}` the cleartext name of the birthday user (eg `HelloWorld`)
- `{{role}}` [EXPERIMENTAL] the birthday role given to the user (if any) (eg `@Birthdays`)"##
)]
#[usage("<format|--clear>")]
#[example("its {{mention}}'s birthday!")]
#[min_args(1)]
#[checks(Admin)]
async fn set_format(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
	use crate::db::{models::Guild, schema::guilds::dsl::*};
	use diesel::prelude::*;

	let gid = match msg.guild_id {
		Some(v) => U64(v.0),
		None => {
			tracing::debug!("not a guild");
			return Ok(());
		},
	};

	let vx = match args.current() {
		Some("--clear") => String::new(),
		Some(_) => args.rest().to_string(),
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "missing argument").await);
			return Err("missing argument".into());
		},
	};

	let db = Arc::clone(
		ctx.data
			.read()
			.await
			.get::<crate::bot::DbConnection>()
			.expect("db connection was never inserted"),
	);

	let res = diesel::update(guilds.filter(id.eq(gid)))
		.set(announce_format.eq(vx))
		.get_result_async::<Guild>(&*db)
		.await;
	match res {
		Ok(v) => {
			let r = if v.announce_format.is_empty() {
				"announce text cleared".to_owned()
			} else {
				format!("announcement text set to \"{}\"", v.announce_format)
			};
			err_log!(msg.channel_id.say(&ctx.http, r).await);
			Ok(())
		},
		Err(AsyncError::Error(diesel::NotFound)) => {
			Err("failed to insert new guild, may need to call init".into())
		},
		Err(o) => Err(format!("unknown error: {:?}", o).into()),
	}
}

#[tracing::instrument(skip(ctx, msg, args))]
#[command("channel")]
#[description("the channel, the birthdays are announced (using the birthday message)")]
#[usage("<user|id|--clear>")]
#[example("12345678")]
#[min_args(1)]
#[checks(Admin)]
async fn set_channel(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
	use crate::db::{models::Guild, schema::guilds::dsl::*};
	use diesel::prelude::*;

	let gid = match msg.guild_id {
		Some(v) => U64(v.0),
		None => {
			tracing::debug!("not a guild");
			return Ok(());
		},
	};

	let vx = match args.current() {
		Some("--clear") => None,
		Some(_) => crate::utils::mention_or_id(args.rest()),
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "missing argument").await);
			return Err("missing argument".into());
		},
	}
	.map(U64);

	let db = Arc::clone(
		ctx.data
			.read()
			.await
			.get::<crate::bot::DbConnection>()
			.expect("db connection was never inserted"),
	);

	let res = diesel::update(guilds.filter(id.eq(gid)))
		.set(announce_in.eq(vx))
		.get_result_async::<Guild>(&*db)
		.await;
	match res {
		Ok(v) => {
			let r = if let Some(ain) = v.announce_in {
				format!("announcement channel set to <#{}>", ain)
			} else {
				"announcement channel cleared".to_owned()
			};
			err_log!(msg.channel_id.say(&ctx.http, r).await);
			Ok(())
		},
		Err(AsyncError::Error(diesel::NotFound)) => {
			let txt = "failed to update guild, may need to call init";
			err_log!(msg.channel_id.say(&ctx.http, txt).await);
			Err(txt.into())
		},
		Err(o) => Err(format!("unknown error: {:?}", o).into()),
	}
}

#[tracing::instrument(skip(ctx, msg, args))]
#[command("role")]
#[description("set the role that is given for 24hrs to the birthday person")]
#[usage("<role|id|--clear>")]
#[example("123135241")]
#[checks(Admin)]
#[min_args(1)]
async fn set_role(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
	use crate::db::{models::Guild, schema::guilds::dsl::*};
	use diesel::prelude::*;

	let gid = match msg.guild_id {
		Some(v) => U64(v.0),
		None => {
			tracing::debug!("not a guild");
			return Ok(());
		},
	};

	let vx = match args.current() {
		Some("--clear") => None,
		Some(_) => crate::utils::mention_or_id(args.rest()),
		None => {
			err_log!(msg.channel_id.say(&ctx.http, "missing argument").await);
			return Err("missing argument".into());
		},
	}
	.map(U64);

	let db = Arc::clone(
		ctx.data
			.read()
			.await
			.get::<crate::bot::DbConnection>()
			.expect("db connection was never inserted"),
	);

	let res = diesel::update(guilds.filter(id.eq(gid)))
		.set(set_role.eq(vx))
		.get_result_async::<Guild>(&*db)
		.await;
	match res {
		Ok(v) => {
			let r = if let Some(ain) = v.set_role {
				format!("awarded role set to <@&{}>", ain)
			} else {
				"awarded role cleared".to_owned()
			};
			err_log!(msg.channel_id.say(&ctx.http, r).await);
			Ok(())
		},
		Err(AsyncError::Error(diesel::NotFound)) => {
			let txt = "failed to update guild, may need to call init";
			err_log!(msg.channel_id.say(&ctx.http, txt).await);
			Err(txt.into())
		},
		Err(o) => Err(format!("unknown error: {:?}", o).into()),
	}
}

#[tracing::instrument]
#[command("remove-role")]
#[description("")]
#[checks(Owner)]
async fn set_remove_role(_: &Context, _: &Message) -> CommandResult {
	// TODO: Implement set_remove_role
	Ok(())
}
