use serenity::{
	framework::standard::{macros::check, Args, CommandOptions},
	model::channel::Message,
	prelude::*,
};
use serenity::framework::standard::Reason;

type CheckResult = Result<(), Reason>;

macro_rules! ok_or_reason {
	($check:expr => Log($log:expr)) => {{
		ok_or_reason!(@derive $check => ::serenity::framework::standard::Reason::Log($log.to_string()))
	}};
	($check:expr => User($user:expr)) => {{
		ok_or_reason!(@derive $check => ::serenity::framework::standard::Reason::User($user))
	}};
	($check:expr => Unknown) => {{
		ok_or_reason!(@derive $check => ::serenity::framework::standard::Reason::Unknown)
	}};
	(@derive $check:expr => $r:expr) => {{
		if $check {
			Ok(())
		} else {
			Err($r)
		}
	}};
}

#[tracing::instrument(skip(msg))]
#[check]
#[name = "Owner"]
#[check_in_help(true)]
#[display_in_help(false)]
pub async fn owner_check(
	_: &Context,
	msg: &Message,
	_: &mut Args,
	_: &CommandOptions,
) -> CheckResult {
	SPAM_S!(ok_or_reason!(msg.author.id == crate::OWNER => Log("owner check")))
}

#[tracing::instrument(skip(ctx, msg))]
#[check]
#[name = "Admin"]
#[check_in_help(true)]
#[display_in_help(true)]
pub async fn admin_check(
	ctx: &Context,
	msg: &Message,
	_: &mut Args,
	_: &CommandOptions,
) -> CheckResult {
	SPAM_S!(ok_or_reason!(msg.author.id == crate::OWNER => Log("admin check (owner)")))?;

	if let Ok(member) = msg.member(&ctx).await {
		if let Ok(permissions) = member.permissions(&ctx).await {
			return SPAM_S!(ok_or_reason!(permissions.administrator() => Log("admin check")));
		}
	}

	SPAM_S!(Err(Reason::Log("admin check".to_string())))
}

#[tracing::instrument(skip(ctx, msg))]
#[check]
#[name = "Mod"]
#[check_in_help(true)]
#[display_in_help(true)]
pub async fn moderator_check(
	ctx: &Context,
	msg: &Message,
	_: &mut Args,
	_: &CommandOptions,
) -> CheckResult {
	SPAM_S!(ok_or_reason!(msg.author.id == crate::OWNER => Log("mod check (owner)")))?;

	if let Ok(member) = msg.member(&ctx).await {
		if let Ok(permissions) = member.permissions(&ctx).await {
			return SPAM_S!(ok_or_reason!(
				(permissions.administrator() || permissions.manage_roles()) =>
				Log("mod check")
			));
		}
	}

	SPAM_S!(Err(Reason::Log("mod check".to_string())))
}
