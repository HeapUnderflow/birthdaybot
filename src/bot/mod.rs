mod checks;
pub mod commands;
pub mod handler;
mod worker;

use self::commands::GENERAL_GROUP;
use crate::db::ConnectionPool;
use serenity::{
	framework::standard::{macros::hook, CommandError, StandardFramework},
	model::channel::Message,
	prelude::*,
	Client,
};
use std::{collections::HashSet, sync::Arc};
use serenity::client::bridge::gateway::GatewayIntents;

/// Marker type to insert `crate::Connection` into a typemap
#[derive(Debug, Clone, Copy)]
pub struct DbConnection;

impl TypeMapKey for DbConnection {
	type Value = ConnectionPool;
}

#[derive(Debug)]
pub enum BotError {
	Message { msg: String },
	Other { error: Box<dyn std::error::Error> },
}

/// Birthday Bot
///
/// new does not actually start the bot. the non-returning function
/// `run_forever` does
pub struct Bot {
	client:  Client,
	_worker: worker::Worker,
}

impl Bot {
	pub async fn new(
		token: &str,
		connection: ConnectionPool,
		known_guilds: Arc<Mutex<crate::KnownGuilds>>,
	) -> Result<Bot, BotResult> {
		// We will fetch your bot's owners and id
		let (owners, bot_id) = match serenity::http::client::Http::new_with_token(token)
			.get_current_application_info()
			.await
		{
			Ok(info) => {
				let mut owners = HashSet::new();
				owners.insert(info.owner.id);

				tracing::debug!(?info.name, ?info.id, "found appinfo");

				(owners, info.id)
			},
			Err(why) => panic!("Could not access application info: {:?}", why),
		};

		// FIXME: use correct intents for standard framework
		let framework = StandardFramework::new()
			.configure(|c| {
				c.with_whitespace(true)
					.on_mention(Some(bot_id))
					.delimiters(vec![" "])
					.owners(owners)
			})
			.after(after)
			.help(&handler::HELP_COMMAND)
			.group(&GENERAL_GROUP);

		let client = Client::builder(token)
			.intents( GatewayIntents::GUILDS | GatewayIntents::GUILD_MESSAGES | GatewayIntents::GUILD_MEMBERS )
			.event_handler(handler::Handler)
			.framework(framework)
			.await
			.map_err(BotResult::ClientError)?;

		let (tx, rx) = crate::ReadySnd::new();
		let worker = worker::Worker::start(
			Arc::clone(&connection),
			Arc::clone(&client.cache_and_http),
			Arc::clone(&known_guilds),
			rx,
		);

		{
			let mut data = client.data.write().await;
			data.insert::<DbConnection>(connection);
			data.insert::<crate::KnownGuilds>(known_guilds);
			data.insert::<crate::ReadySnd>(tx);
		}

		Ok(Bot { client, _worker: worker })
	}

	pub async fn register_ctrlc(&self) -> tokio::task::JoinHandle<()> {
		let hdl = Arc::clone(&self.client.shard_manager);
		tokio::spawn(async move {
			tokio::signal::ctrl_c().await.expect("ctrl-c failed!");
			tracing::info!("ctrl+c received, attempting to shut down shards gracefully.");
			hdl.lock().await.shutdown_all().await;
		})
	}

	// This is consuming the "bot" struct, as we never plan to return from this
	// function
	pub async fn run_forever(mut self) -> serenity::Result<()> { self.client.start().await }
}

#[hook]
async fn after(_: &Context, _: &Message, command_name: &str, error: Result<(), CommandError>) {
	match error {
		Ok(()) => tracing::debug!("Processed command '{}'", command_name),
		Err(why) => tracing::debug!("Command '{}' returned error {:?}", command_name, why),
	}
}

#[derive(Debug, thiserror::Error)]
pub enum BotResult {
	#[error("the supplied token is invalid")]
	InvalidToken,
	#[error("client failure: {0}")]
	ClientError(#[from] serenity::Error),
}
