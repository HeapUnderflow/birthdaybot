use std::{env, fs::File, io, io::Write, path::Path};

fn main() {
	discord_friendly_build_stats(
		Path::new(&env::var("OUT_DIR").unwrap()).join("discord_friendly_build_stats.txt").as_path(),
	)
	.expect("writing build stats failed");
}

fn discord_friendly_build_stats(to: &Path) -> io::Result<()> {
	let mut f = File::create(to)?;

	writeln!(f, "Built for: {}", env::var("TARGET").unwrap())?;
	writeln!(f, "using profile: {}", env::var("PROFILE").unwrap())?;
	writeln!(f, "on date: {}", chrono::Utc::now().format("%a, %e %b %Y %H:%M:%S %Z"))?;
	Ok(())
}
